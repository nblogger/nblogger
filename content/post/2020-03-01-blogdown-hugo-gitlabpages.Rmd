---
title: Blogdown & Hugo & Gitlabpages
author: Nico Blokker
date: '2020-03-01'
slug: blogdown-hugo-gitlabpages
categories: []
tags: []
---

# Workflow

## New template

1. chose theme
2. build new site
    + e.g. academic: `blogdown::new_site(theme = "gcushen/hugo-academic")`
    + or defauult hugo-lithium: `blogdown::new_site()`
3. adjust .toml
    + set `baseurl = "/"`
    + set `relativeURLs = true`
    + set ignorefiles option
4. run `blogdown::hugo_build()`
5. add ci
6. add, commit, push

## Changes

1. `blogdown::new_post("title", ext ='.Rmd')`
2. `blogdown::serve_site()`
3. add, commit, push